defmodule Bonfire.Upcycle.Web.MyIntentsLive do
  use Bonfire.UI.Common.Web, :stateless_component
  prop intents, :any
end
