<section class="relative w-full p-4 bg-base-100">
  {!-- Create a form --}
  <Form
    class="flex flex-col w-full p-2 space-y-3"
    for={e(
      @__context__,
      ValueFlows.Planning.Intent,
      :changeset,
      ValueFlows.Planning.Intent.validate_changeset()
    )}
    opts={autocomplete: "off", "phx-target": "#smart_input"}
    submit="ValueFlows.Planning.Intent:create"
    change="validate"
  >
    <HiddenInput :if={@intent_url} name="redirect_after" value={@intent_url} />
    <HiddenInput name="action_id" value={@action_id} />
    <HiddenInput
      name="resource_inventoried_as"
      value={e(@smart_input_opts, :resource_inventoried_as, nil)}
    />
    <HiddenInput
      name="resource_conforms_to"
      value={e(@smart_input_opts, :resource_conforms_to, nil)}
    />

    <HiddenInput name="classified_as[]" value={Bonfire.Upcycle.remote_tag_id()} />

    <Bonfire.UI.Common.InputControlsLive
      smart_input_opts={@smart_input_opts}
      thread_mode={@thread_mode}
      reply_to_id={uid(@reply_to_id)}
      context_id={@context_id}
      create_object_type={@create_object_type}
      to_boundaries={@to_boundaries}
      to_circles={@to_circles}
      exclude_circles={@exclude_circles}
      showing_within={@showing_within}
      uploads={@uploads}
      uploaded_files={@uploaded_files}
    >
      <div hidden="true">
        <Field name={:has_beginning}>
          {{:ok, datetime} = DateTime.now("Etc/UTC-6")
          datetime}
          <DateTimeLocalInput name="has_beginning" opts={required: true} value={datetime} />
        </Field>
      </div>

      <fieldset class="mt-2">
        <legend class="text-sm font-normal text-base-content/70">{l("Type")}</legend>
        <div class="flex items-center mt-2 border rounded-lg border-base-content/20 bg-base-100">
          <div class="flex items-center flex-1 p-3">
            <input
              id="Need"
              name="type"
              type="radio"
              value="need"
              checked={(e(@smart_input_opts, :intent_type, nil) || @intent_type) in [:need, "need"]}
              class="w-5 h-5 radio"
            />

            <label for="Need" class="block ml-3 text-sm text-base-content/70">
              {l("I need")}
            </label>
          </div>
          <div class="flex items-center flex-1 p-3 border-l border-base-content/10">
            <input
              id="Offer"
              name="type"
              type="radio"
              value="offer"
              checked={(e(@smart_input_opts, :intent_type, nil) || @intent_type) in [:offer, "offer"]}
              class="w-5 h-5 radio"
            />
            <label for="Offer" class="block ml-3 text-sm text-base-content/70">
              {l("I offer")}
            </label>
          </div>
        </div>
      </fieldset>

      {!-- Create a title input form field --}
      <Field class="w-full p-2 pt-0 m-0" name={:name}>
        <Label class="block text-sm text-base-content/80">{l("Title")}</Label>
        <TextInput
          opts={required: true, placeholder: "Type a title"}
          class="input input-bordered h-[2.5rem] w-full mt-2"
        />
      </Field>
      <Inputs for={:resource_quantity}>
        <div class="w-full p-2 pt-0 m-0">
          <Label class="block text-sm text-base-content/80">{l("Quantity")}</Label>
          <div class="grid grid-cols-2 mt-2">
            <Field
              name={:has_numerical_value}
              class="w-full border border-r-0 rounded-l-lg border-base-content/20"
            >
              <NumberInput
                value={e(@reply_to_id, :resource_quantity, :has_numerical_value, nil) ||
                  e(@reply_to_id, :resource_quantity, nil)}
                opts={placeholder: "00.00", min: 1, required: true}
                class="w-full input h-[2.5rem]"
              />
            </Field>
            <Field name={:has_unit} class="w-full border rounded-r-lg border-base-content/20">
              <Select
                options={units()}
                selected={e(@reply_to_id, :unit_id, nil)}
                class="w-full select !min-h-0 h-[2.5rem]"
              />
            </Field>
          </div>
        </div>
      </Inputs>
      {!-- <div class="w-full p-2 pt-0 m-0 rounded-lg shadow-inner bg-base-300">
      <Label class="block text-sm text-base-content/80">{l("Offer Expires")}</Label>
      <Field name={:due}>
        <DateTimeLocalInput
          name="due"
          opts={required: true}
          class="block w-full input text-base-content rounded-box focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </Field>
    </div> --}
      <Field name={:note} class="w-full p-2">
        <Label class="block text-sm text-base-content/80">{l("Write an optional description")}</Label>
        <TextArea class="w-full mt-2 textarea textarea-bordered" />
      </Field>
    </Bonfire.UI.Common.InputControlsLive>

    {!-- <button type="submit" class="w-full btn btn-primary">{l("Publish")}</button> --}
  </Form>
</section>