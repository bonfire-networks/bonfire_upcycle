<Form
  class="p-4"
  for={@changeset || ValueFlows.EconomicEvent.validate_changeset()}
  opts={autocomplete: "off", "phx-target": "#smart_input"}
  submit="ValueFlows.EconomicEvent:create"
  change="Bonfire.UI.ValueFlows.CreateEconomicEventLive:autocomplete"
>
  <HiddenInput name="action" value={@action} />
  <HiddenInput :if={@input_of_id} name="input_of_id" value={@input_of_id} />
  <HiddenInput :if={@output_of_id} name="output_of_id" value={@output_of_id} />
  <HiddenInput name="redirect_after" value={~p"/upcycle/transfers?created="} />

  {!-- Submit Button --}
  {!-- <div class="flex justify-center">
    <button
      type="submit"
      class="text-base font-normal normal-case bg-indigo-600 border-0 rounded-md btn hover:bg-indigo-400"
    >
      {l("Create Transfer")}
    </button>
  </div> --}

  <Bonfire.UI.Common.InputControlsLive
    smart_input_opts={@smart_input_opts}
    thread_mode={@thread_mode}
    reply_to_id={uid(@reply_to_id)}
    context_id={@context_id}
    create_object_type={@create_object_type}
    to_boundaries={@to_boundaries}
    to_circles={@to_circles}
    exclude_circles={@exclude_circles}
    showing_within={@showing_within}
    uploads={@uploads}
    uploaded_files={@uploaded_files}
    submit_label={l("Submit")}
  >
    {!-- From --}
    <div class="grid grid-cols-2 p-3 border rounded border-base-content/10">
      <div class="">
        <div class="mb-2 text-sm font-semibold text-base-content/70">
          {l("From")}
        </div>
        <LinkLive to={path(current_user(@__context__))} class="flex items-center gap-3">
          <Bonfire.UI.Common.AvatarLive
            parent_id="create_transfer"
            class="w-8 h-8 rounded-lg bg-base-content/5"
            user={e(current_user(@__context__), :profile, nil)}
          />
          <div class="text-lg font-semibold link link-hover">{e(current_user(@__context__), :profile, :name, "Unknown name")}</div>
        </LinkLive>
      </div>
      {!-- To --}
      <div class="">
        <div class="mb-2 text-sm font-semibold text-base-content/70">
          {l("To")}
        </div>
        <Bonfire.UI.Common.MultiselectLive.UserSelectorLive
          label="Select a receiver"
          form_input_name="receiver"
          preloaded_options={@users_autocomplete}
          selected_options={@receiver}
        />
      </div>
    </div>

    {!-- Resource (Bonfire Component) --}
    {!-- <div class="p-2 m-1 rounded-md">
    {l "Specify which resource to transfer"}
        <div class="relative flex-1">
            <Bonfire.UI.Common.MultiselectLive
            preloaded_options={e(@economic_resources_autocomplete, [])}
            selected_options={e(@economic_resource_selected, [])} 
            label={"Pick a resource..."}
            form_input_name={"resource_inventoried_as"}
            pick_event={e(@pick_event, "ValueFlows.EconomicResource:select")}
            remove_event={@remove_event}
            focus_event="ValueFlows.EconomicResource:autocomplete"
            />
        </div>
    </div> --}

    <div class="grid grid-cols-2 p-3 mt-3 border rounded border-base-content/10">
      {!-- Quantity and Units --}
      <div class="">
        <div class="mb-2 text-sm font-semibold text-base-content/70">
          {l("Amount")}
        </div>
        <div class="flex items-center">
          <Inputs for={:resource_quantity}>
            <Field name={:has_numerical_value} class="flex-grow">
              <NumberInput
                value={@resource_quantity || e(@reply_to_id, "resource_quantity", nil)}
                placeholder="0.0"
                opts={required: true, min: 1, max: @resource_quantity}
                class="input w-full input-bordered h-[2.5rem]"
              />
            </Field>

            <Field name={:has_unit}>
              <HiddenInput
                value={@unit_id || e(@reply_to_id, "unit_id", nil)}
                class="rounded-tr-lg rounded-br-lg select"
              />
            </Field>
            {!-- <Field name={:has_unit}>
            <Select options={units()} class="rounded-tr-lg rounded-br-lg select" />
          </Field> --}

            <span class="mx-3">{@unit_name || e(@reply_to_id, "unit_name", nil)} {l("of")}</span>
          </Inputs>
        </div>
      </div>

      {!-- Resource Drop-down --}
      {!-- <Field name={:resource_inventoried_as}>
    <HiddenInput value={@resource_id} />
  </Field> --}
      <input
        name="economic_event[resource_inventoried_as]"
        value={@resource_id || uid(@reply_to_id)}
        type="hidden"
      />

      <div id="select_resource" x-data="{ open: false }">
        <Field name={:resource_name} class="">
          <div class="mb-2 text-sm font-semibold text-base-content/70">
            {l("Resource")}
          </div>
          <div class="flex w-full h-full" x-on:click="open = !open">
            <TextInput
              value={@resource_name || e(@reply_to_id, "name", nil)}
              opts={required: true, placeholder: "Select a resource"}
              class="w-full  h-[2.5rem] input input-bordered"
              disabled={if not is_nil(e(@reply_to_id, "name", nil)), do: "disabled"}
            />
          </div>
          <div x-show="open" x-on:click.away="open = false">
            {#if @resources || e(@smart_input_opts, :resources, nil)}
              {#for %{
                  id: id,
                  name: name,
                  onhand_quantity: %{has_numerical_value: value, has_unit: %{id: unit_id, label: unit}}
                } <-
                  @resources || e(@smart_input_opts, :resources, nil)}
                {#if value > 0}
                  <div>
                    <label
                      x-on:click="open = false"
                      phx-value-resource_id={id}
                      phx-value-resource_name={name}
                      phx-value-resource_quantity={value}
                      phx-value-unit_id={unit_id}
                      phx-value-unit_name={unit}
                      phx-click="assign"
                      phx-target="#select_resource"
                      class="block p-1 border-2 rounded-md hover:bg-blue-500 hover:text-white"
                    >
                      {name} - {value} {unit}
                    </label>
                  </div>
                {/if}
              {/for}
            {/if}
          </div>
        </Field>
      </div>
    </div>

    {!-- Description --}
    <div class="mt-3">
      <div class="mb-2 text-sm font-semibold text-base-content/70">
        {l("Add an optional note about the transfer")}
      </div>

      <Field name={:resource_note}>
        <TextArea class="w-full rounded-md textarea-bordered textarea" />
      </Field>
    </div>
  </Bonfire.UI.Common.InputControlsLive>
</Form>