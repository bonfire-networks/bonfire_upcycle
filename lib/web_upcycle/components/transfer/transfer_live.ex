defmodule Bonfire.Upcycle.Web.TransferLive do
  use Bonfire.UI.Common.Web, :stateless_component

  prop event, :any
end
