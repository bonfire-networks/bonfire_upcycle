defmodule Bonfire.Upcycle.Web.DiscoverLive do
  use Bonfire.UI.Common.Web, :stateless_component
  prop intents, :any
end
