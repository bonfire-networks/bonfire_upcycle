defmodule Bonfire.Upcycle.Web.MainNavigationLive do
  use Bonfire.UI.Common.Web, :stateless_component

  prop page, :string
end
