# Bonfire:Upcycle

Upcycle supports a network of high school fablabs in Milwaukee that receive contributions of materials from organizations in the community and share these with each other. It is implemented as a UI for [ValueFlows](https://github.com/bonfire-networks/bonfire_valueflows), which are both available as extensions for [Bonfire](https://bonfire.cafe/). Upcycle was initially created for [LearnDeep](https://learndeep.org/) by students at [MSOE](https://www.msoe.edu/), and is now being maintained by the Bonfire team.

## Set up
Upcycle is packaged as part of the `cooperation` flavour of Bonfire. Check out the development or deployment [docs](https://doc.bonfirenetworks.org/readme.html) to get started.

## Copyright and License

Copyright (c) 2022 all contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see <https://www.gnu.org/licenses/>.
